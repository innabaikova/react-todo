import React, { Component } from "react";
import "./app.css";
import AppHeader from "../app-header";
import SearchPanel from "../search-panel";
import ItemStatusFilter from "../item-status-filter";
import TodoList from "../todo-list";
import ItemAddForm from "../item-add-form";

export default class App extends Component {
  maxId = 0;

  createItem = (label) => {
    return {
      id: this.maxId++,
      label,
      important: false,
      done: false,
    };
  };

  state = {
    todoData: [
      this.createItem("Drink Coffee"),
      this.createItem("Make awesome app"),
      this.createItem("Have a lunch"),
    ],
    term: "",
    filter: "all",
  };

  deleteItem = (id) => {
    this.setState(({ todoData }) => ({
      todoData: todoData.filter((item) => item.id !== id),
    }));
  };

  addItem = (label) => {
    const item = this.createItem(label);

    this.setState(({ todoData }) => ({
      todoData: [...todoData, item],
    }));
  };

  toggleProperty = (arr, id, property) => {
    return arr.map((item) => {
      if (item.id !== id) return item;
      return { ...item, [property]: !item[property] };
    });
  };

  toggleImportant = (id) => {
    this.setState(({ todoData }) => ({
      todoData: this.toggleProperty(todoData, id, "important"),
    }));
  };

  toggleDone = (id) => {
    this.setState(({ todoData }) => ({
      todoData: this.toggleProperty(todoData, id, "done"),
    }));
  };

  updateTerm = (term) => this.setState({ term });

  setFilter = (filter) => this.setState({ filter });

  search = (arr, str) => {
    if (!str) return arr;

    return arr.filter(({ label }) =>
      label.toLowerCase().includes(str.toLowerCase())
    );
  };

  filter = (arr, mode) => {
    switch (mode) {
      case "active":
        return arr.filter((item) => !item.done);
      case "done":
        return arr.filter((item) => item.done);
      default:
        return arr;
    }
  };

  render() {
    const { todoData, term, filter } = this.state;
    const visibleData = this.search(this.filter(todoData, filter), term);
    const doneCount = todoData.filter((item) => item.done).length;
    const toDoCount = todoData.length - doneCount;

    return (
      <div className='todo-app'>
        <AppHeader toDo={toDoCount} done={doneCount} />
        <div className='top-panel d-flex'>
          <SearchPanel onSearchChange={this.updateTerm} />
          <ItemStatusFilter onFilter={this.setFilter} filter={filter}/>
        </div>
        <TodoList
          todos={visibleData}
          onDeleted={this.deleteItem}
          onToggleImportant={this.toggleImportant}
          onToggleDone={this.toggleDone}
        />
        <ItemAddForm onAdded={this.addItem} />
      </div>
    );
  }
}
