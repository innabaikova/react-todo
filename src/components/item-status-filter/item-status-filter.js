import React, { Component } from "react";

export default class ItemStatusFilter extends Component {
  buttons = [
    { name: "all", label: "All" },
    { name: "active", label: "Active" },
    { name: "done", label: "Done" },
  ];

  setMode = (mode) => {
    this.setState({ mode });
    this.props.onFilter(mode);
  };

  render() {
    const buttons = this.buttons.map(({ name, label }) => {
      const clazz =
        name === this.props.filter ? "btn-info" : "btn-outline-secondary";
      return (
        <button
          key={name}
          type='button'
          className={`btn ${clazz}`}
          onClick={() => this.setMode(name)}>
          {label}
        </button>
      );
    });

    return <div className='btn-group'>{buttons}</div>;
  }
}
